.PHONY: i3 common desktop dev installi3 i3

stow = cd config && stow -v -t ~

ohmyzsh:
	sudo rm -rf ~/.oh-my-zsh 
	sh -c "$$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

flatpak:
	sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

all: common desktop dev i3

common: flatpak
	sed -e 's:#.*$$::g' -e '/^flatpak\./d' packages/common.list | xargs sudo apt install -y
	sed -e 's:#.*$$::g' -e '/^flatpak\./p' -n packages/common.list | sed 's:^flatpak\.::g' | xargs sudo flatpak install -y flathub
	$(stow) ssh
	$(stow) vim
	$(stow) git
	$(stow) screen
	$(stow) tmux
	mv ~/gitignore ~/.gitignore
	
desktop: flatpak
	sed -e 's:#.*$$::g' -e '/^flatpak\./d' packages/desktop.list | xargs sudo apt install -y
	sed -e 's:#.*$$::g' -e '/^flatpak\./p' -n packages/desktop.list | sed 's:^flatpak\.::g' | xargs sudo flatpak install -y flathub

dev: flatpak
	sed -e 's:#.*$$::g' -e '/^flatpak\./d' packages/dev.list | xargs sudo apt install -y
	sed -e 's:#.*$$::g' -e '/^flatpak\./p' -n packages/dev.list | sed 's:^flatpak\.::g' | xargs sudo flatpak install -y flathub

i3: flatpak
	sed -e 's:#.*$$::g' -e '/^flatpak\./d' packages/i3.list | xargs sudo apt install -y
	sed -e 's:#.*$$::g' -e '/^flatpak\./p' -n packages/i3.list | sed 's:^flatpak\.::g' | xargs sudo flatpak install -y flathub
	$(stow) urxvt
	xrdb ~/.Xresources
	$(stow) i3
	$(stow) polybar
	$(stow) picom
	$(stow) dunst
	$(stow) rofi
	$(stow) thunar
