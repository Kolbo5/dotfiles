#!/bin/sh

# (?'debut'[ \[])(?'byte'b(?'quote'["']).*?\k'quote')+?(?'fin'[,\]])

sed -r "s;: b'([^\")]*)';: \"byte\";g;s;: b\"([^\")]*)\";: \"byte\";g" | \
sed -r "s;: \[b'([^\")]*)', ;: \[\"byte\", ;g;s;: \[b\"([^\")]*)\", ;: \[\"byte\", ;g" | \
sed -r "s;, b'([^\")]*)', ;, \"byte\", ;g;s;, b\"([^\")]*)\", ;, \"byte\", ;g" | \
sed -r "s;, b'([^\")]*)'\];, \"byte\"\];g;s;, b\"([^\")]*)\"\];, \"byte\"\];g" | \
sed -r "s;\\\x;x;g" | \
sed -r "s;ObjectId\('([^\)]*)'\);{\"\$oid\": \"\1\"};g" | \
sed -r "s;: datetime.datetime\(([^\)]*)\);: \"\1\";g" | \
sed -r "s;'([^']*)': ;\"\1\": ;g;s;: '([^']*)';: \"\1\";g" | \
sed -r "s;False;\"false\";g;s;True;\"true\";g;s;None;\"none\";g" | \
sed -r "s;';\";g"