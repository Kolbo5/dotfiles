#!/bin/bash

print_help() {
	printf 'Usage: %s [-h] [-d]\n' "$script_name"
	printf '...\n'
	printf '\nArguments:\n'
	printf ' -h            Prints this help page.\n'
}

main() {
	local debug uniq='' trim='cat'
	parse_aruments "$@"
	echo "Premier lancement"
	while read -r json; do
	#if [ "$(jq -r '.|select(.change|contains("new","move","close","init"))|.change' <<< "$json" | wc -l)" -gt 0 ]; then
		echo "Lancement parce que qqc a changé dans i3 :"
        echo $json
        echo "--------------"
	#fi
	done < <(i3-msg -mt subscribe '["window","workspace"]')
}

parse_aruments() {
	local script_name
	script_name="$(basename "$0")"
	while getopts hd: arg; do
		case $arg in
			h) print_help; exit 0;;
			d) debug=true;;
			*) print_help; exit 1;;
		esac
	done

	shift $((OPTIND - 1))

	if [ -n "$*" ]; then
		print_help; exit 1;
	fi
}


debug() {
	if [ -n "$debug" ]; then
		printf '%s\n' "$*"
	fi
}

main "$@"
