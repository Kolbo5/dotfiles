# À faire à la main une fois l'install auto finie
Il faudrait voir pour ajouter tout ça à l'automatisation...

## zsh
### powerlevel10k
c'est un thème pour zsh
```zsh
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```
et vérifier qu'il y a bien :
```zsh
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
```
dans le .zshrc

Il faudrait aussi finir l'automatisation de l'install de powerlevel10k, notemment les fonts (https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k)

### thefuck
```zsh
pip3 install thefuck --user
```
### k
C'est un ls qui gère git, c'est sympa (voir si on remplace exa ou pas...)
```zsh
git clone https://github.com/supercrabtree/k $ZSH_CUSTOM/plugins/k
```

### zsh-syntax-highlighting
```zsh
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```
et vérifier que `zsh-syntax-highlighting` est dans les plugins zsh

### zsh-autosuggestions
```zsh
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```
et vérifier que `zsh-autosuggestions` est bien dans les plugin de .zshrc (mais normalement c'est bon)

## fish
https://fishshell.com/
et
https://github.com/jorgebucaran/awsm.fish

### installer la derniere version de fish
```sh
sudo apt-add-repository ppa:fish-shell/release-3
sudo apt update
sudo apt install fish
```
https://fishshell.com/docs/current/#default-shell
```sh
echo $(which fish) | sudo tee -a /etc/shells
chsh -s $(which fish)
```

### Récupérer l'historique de zsh dans l'historique fish
https://pypi.org/project/zsh-history-to-fish/
```sh
pip install zsh-history-to-fish
zsh-history-to-fish
```

### installer un gestionnaire de plugins : fisher
https://github.com/jorgebucaran/fisher
```sh
curl -sL https://raw.githubusercontent.com/jorgebucaran/fisher/main/functions/fisher.fish | source && fisher install jorgebucaran/fisher
```

### Un prompe sympa : Tide
https://github.com/IlanCosman/tide

#### ajouter les bonne polices de caratère
https://github.com/IlanCosman/tide?tab=readme-ov-file#meslo-nerd-font

1) Télécharger les 4.
2) Double cliquer dessus puis "install"
3) ajouter les lignes suivantes au fichier `.Xresources` :
```
URxvt.font:             xft:MesloLGS NF:size=11
URxvt*boldFont:		    xft:MesloLGS NF:bold:size=11
```
4) reboot

#### install
```sh
fisher install IlanCosman/tide@v6
```
puis configurer
```sh
tide configure --auto --style=Rainbow --prompt_colors='True color' --show_time='24-hour format' --rainbow_prompt_separators=Angled --powerline_prompt_heads=Sharp --powerline_prompt_tails=Flat --powerline_prompt_style='Two lines, character' --prompt_connection=Dotted --powerline_right_prompt_frame=No --prompt_connection_andor_frame_color=Dark --prompt_spacing=Sparse --icons='Many icons' --transient=No
```

### z
https://github.com/jethrokuan/z
```sh
fisher install jethrokuan/z
```

### fish-exa
https://github.com/gazorby/fish-exa
`exa` est déjà installé depuis apt noralement.
Ajouter les alias inclus dans fish en installant `fish-exa` via fisher :
```sh
fisher install gazorby/fish-exa
```

### thefuck
```zsh
pip3 install thefuck --user
```



### fzf.fish
https://github.com/PatrickF1/fzf.fish

#### vérifier que la versione de fish est assez grande
Elle doit être suppérieure à 3.4.0

#### installer fzf > 0.33.0
https://github.com/junegunn/fzf

```sh
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
~/.fzf/install
```

### installer fd > 8.5.0
https://github.com/sharkdp/fd?tab=readme-ov-file#installation

dl la dernière version sur les release github

https://github.com/sharkdp/fd/releases

fd_10.1.0_amd64.deb

puis l'installer
```sh
sudo apt install ./fd_10.1.0_amd64.deb
```

### installer bat > 0.16.0
https://github.com/sharkdp/bat?tab=readme-ov-file#on-ubuntu-using-apt

dl la dernière version sur les release github

https://github.com/sharkdp/bat/releases

bat_0.24.0_amd64.deb

puis l'installer
```sh
sudo apt install ./bat_0.24.0_amd64.deb
```

### Et enfin, installer le plugin fzf.fish


### done
https://github.com/franciscolourenco/done

Just go on with your normal life. You will get a notification when a process takes more than 5 seconds finish, and the terminal window not in the foreground.
```sh
fisher install franciscolourenco/done
```

### GitNow
https://github.com/joseluisq/gitnow

GitNow contains a command set that provides high-level operations on the top of Git.

```sh
fisher install joseluisq/gitnow@2.12.0
```

### Sponge
https://github.com/meaningful-ooo/sponge

Sponge quietly runs in the background and keeps your shell history clean from typos, incorrectly used commands and everything you don't want to store due to privacy reasons.
```sh
fisher install meaningful-ooo/sponge
```

### autopair
https://github.com/jorgebucaran/autopair.fish

Automatically insert, erase, and skip matching pairs as you type in the command-line: (), [], {}, "", and ''. E.g., pressing ( inserts () and positions the cursor in between the parentheses. Hopefully.
```sh
fisher install jorgebucaran/autopair.fish
```

### fish-abbreviation-tips
https://github.com/Gazorby/fish-abbreviation-tips

It helps you remember abbreviations and aliases by displaying tips when you can use them.
```sh
fisher install gazorby/fish-abbreviation-tips
```

### fish-ai a l'air sympa aussi
https://github.com/Realiserad/fish-ai

a voir...


## install i3
Finir par l'install de quelques dépendances, notemment i3 :
```zsh
pip3 install i3-py
```

## install pyenv
```zsh
curl https://pyenv.run | bash
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
```
Ensuite installer des versions de Python avec
```zsh
pyenv install <version_python>
```

Créer des venv avec
```zsh
pyenv virtualenv <version python déjà installée> <nom env>
```

Et pour le chargement automatique du venv, ajouter dans le dossier du projet un fichier `.python-version` qui contiens :
```python
# <nom du venv> venv in <python version> version :
<nom du venv>
```

## dunst (application de notifications)
C'est déjà installé via apt-get, mais en faisant les actions en dessous on build et installe la dernière version
### install dependencies
```zsh
sudo apt install libdbus-1-dev libx11-dev libxinerama-dev libxrandr-dev libxss-dev libglib2.0-dev libpango1.0-dev libgtk-3-dev libxdg-basedir-dev libnotify-dev
```
### clone the repository
```zsh
git clone https://github.com/dunst-project/dunst.git
cd dunst
```
### compile and install
```zsh
make
sudo make install
```

## autres applis
### tldr
s'installe via pip
```
pip3 install tldr
```
### explainshell-cli
Dans le même genre, une cli pour explainshell peut être interéssante :
Copier le contenu de https://raw.githubusercontent.com/KevCui/explainshell-cli/master/explainshell.sh dans un fichier explainshell.sh dans .local/bin (et lui donner les droit d'execution)

### tmuxp
un gestionnaire de sessions tmux
s'installe via pip
```
pip3 install tmuxp
```

### sshrc
https://supporthost.com/sshrc/#sshrc_what_it_is_and_how_we_can_use_it
```zsh
cd /tmp
wget https://raw.githubusercontent.com/cdown/sshrc/master/sshrc
chmod +x sshrc
sudo mv sshrc /usr/local/bin
```
Puis ajouter l'alias :
```zsh
alias ssh='sshrc'
```
(ça doit déjà être bon avec le fichier alias.zsh de oh-my-zsh installé)

### sshs
gestionnaire de sessions ssh qui se base sur `~/.ssh/config`
https://github.com/quantumsheep/sshs

```zsh
cd /tmp
wget https://github.com/quantumsheep/sshs/releases/download/4.3.0/sshs-linux-amd64
chmod +x sshs-linux-amd64
mv sshs-linux-amd64 sshs
sudo mv sshs /usr/local/bin
```

### autorandr et le service pour le lancer automatiquement
#### autorandr
https://github.com/phillipberndt/autorandr
```bash
sudo pip install "git+http://github.com/phillipberndt/autorandr#egg=autorandr"
```
Puis faire des `autorandr --save <nom_config>` pour les config à garder en mémoire

#### Ajouter un hook postswitch :
Créer un lien symbolique dans `` vers ``
```bash
mkdir ~/.config/autorandr/postswitch.d
echo '#! /bin/sh\nnotify-send -i display "Display profile" "$AUTORANDR_CURRENT_PROFILE"' > ~/.config/autorandr/postswitch.d/00-notify && chmod +x ~/.config/autorandr/postswitch.d/00-notify
# echo 'feh --bg-scale /home/eolivier/Images/fond_ecrans/7dfe9d2f3f9c51eb.jpeg' > ~/.config/autorandr/postswitch.d/10-wallpapers && chmod +x ~/.config/autorandr/postswitch.d/10-wallpapers
echo '#! /bin/sh\nkillall -9 polybar\nsleep 1\npython3 ~/.config/i3/i3_switch_workspace.py' > ~/.config/autorandr/postswitch.d/20-restart-wm && chmod +x ~/.config/autorandr/postswitch.d/20-restart-wm

echo '#! /bin/sh\n\nsleep 1' > ~/.config/autorandr/predetect && chmod +x ~/.config/autorandr/predetect
```

#### Le sercice pour lancer automatiqumement autorandr quand y'a un changement d'écran
https://github.com/smac89/autorandr-launcher
```bash
git clone https://github.com/smac89/autorandr-launcher
sudo make install
systemctl enable --user --now autorandr_launcher.service
journalctl --follow --identifier='autorandr-launcher-service'
```

# ollama
https://github.com/ollama/ollama
## install
```bash
curl -fsSL https://ollama.com/install.sh | sh
```
## probleme de place
ollama garde les modeles dans `/usr/share/ollama/.ollama/models`. S'il n'y a pas assez de place sur `/usr` c'est chiant.

Dans ce cas :
```bash
rm -rf /usr/share/ollama/.ollama/models
mkdir -p ~/.ollama/models
# sudo mount --bind ~/.ollama/models /usr/share/ollama/.ollama/models
sudo ln -s ~/.ollama/models /usr/share/ollama/.ollama/models
alias
```

# A regarder encore

https://i3-discuss.i3.zekjur.narkive.com/sU2meCuJ/i3-assign-workspaces-to-outputs-dynamically

En gros il faut modifier la conf i3 à coup de sed, puis restart (à coup de i3-msg restart)

```zsh
╭─ /tmp ─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── х INT ─╮
╰─❯ cat test                                                                                                                                                                              ─╯
workspace
deux
trois
workspace 2 output DP

╭─ /tmp ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────╮
╰─❯ sed "/^workspace 2/ c \\AAAAAAAA" test                                                                                                                                                ─╯
workspace
deux
trois
AAAAAAAA
```

# Liens à re-regarder un de ces 4

## direnv
avec un fichier `.envrc` dans un dossier qui permet de récupérer des variables d'env suivant le dossier où on est, pas mal.
Si je me lance, s'ajoute cet alias :
```sh
function da -d "Allow or disallow .envrc after printing it."
    echo ------------------------------------------------
    cat .envrc
    echo ------------------------------------------------
    echo "To allow, hit Return."
    read answer
    direnv allow
end
```

## configurer fish et python pip pour ne pas accepter d'install en dehors d'un venv :
dans le .zshrc ou la conf fish :
```sh
# This prevents me from installing packages with pip without being
# in a virtualenv first.
#
# This allows me to keep my system Python clean, and install all my
# packages inside virtualenvs.
#
# See https://docs.python-guide.org/dev/pip-virtualenv/#requiring-an-active-virtual-environment-for-pip
# See https://blog.glyph.im/2023/08/get-your-mac-python-from-python-dot-org.html#and-always-use-virtual-environments
#
set -g -x PIP_REQUIRE_VIRTUALENV true
```

`export PIP_DOWNLOAD_CACHE=$HOME/.pip/cache` a l'air sympa aussi

## une conf tmux bien sympa
https://github.com/gpakosz/.tmux

## une conf vim qui a l'air cool
https://github.com/sd65/MiniVim

## Un gestionaire de dotfile
Regarder si ça pourrait pas être sympa de bouger ce repo dasn ce gestionnaire
https://github.com/twpayne/chezmoi

## autres (pourquoi je les avais mis de côté eux ?)
https://github.com/Grafikart/dotfiles/blob/master/packages/package.list

https://i.redd.it/1vvjekwj7e091.png

Voir un jour si easystroke (actions sur mouvement de souris) pourrais pas être sympa à utiliser

## A regarder
https://blog.stephane-robert.info/docs/outils/indispensables/
notamment https://blog.stephane-robert.info/docs/outils/systeme/asdf-vm/