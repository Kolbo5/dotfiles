#!/bin/sh

PROJECTS="$(docker compose ls -a -q)"

for project in $PROJECTS; do
    running="$(docker compose ls -a | grep $project | grep 'running(')"
    exited="$(docker compose ls -a | grep $project | grep 'exited(')"
    dead="$(docker compose ls -a | grep $project | grep 'dead(')"

    if [ "X${running}" != "X" ]; then
        couleur="00FF00" # vert
        if [ "X${exited}" != "X" ]; then
            couleur="FF9900" # orange
        fi
        if [ "X${dead}" != "X" ]; then
            couleur="FF0000" # rouge
        fi
    else
        if [ "X${exited}" != "X" ]; then
            couleur="111111" # gris
        fi
        if [ "X${dead}" != "X" ]; then
            couleur="FF0000" # rouge
        fi
    fi

    output="$output %{F#$couleur}$project %{F-}"
done

echo "$output"