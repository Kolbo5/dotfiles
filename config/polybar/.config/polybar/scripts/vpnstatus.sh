#!/bin/sh

output="no vpn"

if [ -f ~/mount_tunnel.sh ]; then
    CONNECTED_FIRST=$(bash ~/mount_tunnel.sh status | grep "1 up" || nmap jira.stormshield.eu --system-dns -PN -p ssh 2>/dev/null | grep "Host is up")
    CONNECTED_SECOND=$(nmap tpmiv01l --system-dns -PN -p ssh | grep open)

    green="00FF00"
    orange="FF9900" # orange
    red="FF0000"
    gray="BBBBBB"
    color="$red"

    first="%{F#$red}%{F-}"
    second="%{F#$red}%{F-}"

    if [ -n "${CONNECTED_FIRST}" ]; then
        first="%{F#$green}%{F-}"
        color="$orange"
        if [ -n "${CONNECTED_SECOND}" ]; then
            second="%{F#$green}%{F-}"
            color="$gray"
        fi
    fi

    output="%{F#$color}VPN:%{F-}$first%{F#$color}|%{F-}$second"
fi

echo "$output"