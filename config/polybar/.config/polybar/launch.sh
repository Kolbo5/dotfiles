#!/usr/bin/env bash

# Add this script to your wm startup file.
DIR="$HOME/.config/polybar"

# Terminate already running bar instances
polybar-msg cmd quit

# killall -q polybar

# Wait until the processes have been shut down
# while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
# caution: other process have polybar in theyr names...

# Launch the bar on all monitors
for m in $(polybar --list-monitors | cut -d":" -f1); do
  tp=""
  if [ "$m" == "DP-2-3" ]; then
    tp="right"
  fi
  if [ "$m" == "DP-4.1" ]; then
    tp="right"
  fi
  if [ "$m" == "eDP-1-1" ]; then
    tp="right"
  fi
  if [ "$m" == "eDP-1" ]; then
    tp="right"
  fi
  if [ "$m" == "DP-1-3" ]; then
    tp="right"
  fi
  echo "$m"
  echo "---" | tee -a /tmp/polybar_${m}_top.log /tmp/polybar_${m}_bottom.log
  MONITOR=$m TRAY_POS=$tp polybar -q top -c "$DIR"/config.ini 2>&1 | tee -a /tmp/polybar_${m}_top.log & disown
  MONITOR=$m polybar -q bottom -c "$DIR"/config.ini 2>&1 | tee -a /tmp/polybar_${m}_bottom.log & disown
  echo "Bars launched..."


done

# IPC settings and test
ln -sf /tmp/polybar_mqueue.$! /tmp/ipc-main
echo message >/tmp/ipc-main

