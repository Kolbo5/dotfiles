# bindkey
# Lancer `bindkey` permet d'avoir la liste de tous les raccourcis
# Lancer `bindkey -L` permet d'avoir la liste de tous les raccourcis de zsh

## Ctrl+U       Supprime la ligne
bindkey "^U"    backward-kill-line
bindkey "^u"    backward-kill-line

## Esc+U        met en UPPERCASE le mot suivant
## Esc+L        met en lowercase le mot suivant
bindkey "^[l"   down-case-word
bindkey "^[L"   down-case-word

# alt+<- | alt+->    avance ou reculle d'un mot
bindkey "^[^[[D" backward-word
bindkey "^[^[[C" forward-word

# ctrl+<- | ctrl+->    avance ou reculle d'un mot
bindkey "^[Od" vi-backward-blank-word
bindkey "^[Oc" vi-forward-blank-word

# crlt+alt+<- | crlt+alt+->
bindkey "^[^[Od" beginning-of-line
bindkey "^[^[Oc" end-of-line
