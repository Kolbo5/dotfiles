alias ls='ls --group-directories-first --time-style=+"%Y-%m-%d %H:%M" --color=tty -F'

# Pour utiliser toujours sshrc
alias ssh='sshrc'

# Lancer directement les fichier avec le bon programme suivant l'extention
alias -s xml=code
alias -s log=code
alias -s html=firefox

# Gives you what is using the most space. Both directories and files. Varies on current directory

alias most='du -hsx * | sort -rh | head -10'

# Ajouter une commande pour copier/coller dans le terminal et les pipes
alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'

# Ajouter de la coloration aux commandes habituelles

# avec grc
[[ -s "/etc/grc.zsh" ]] && source /etc/grc.zsh

# avec exa pour ls
alias ls="exa --group-directories-first"

# avec batcat pour cat
alias cat="batcat"
