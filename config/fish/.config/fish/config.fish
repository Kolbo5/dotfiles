if status is-interactive
    # Commands to run in interactive sessions can go here
end

thefuck --alias | source


# pyenv init
if command -v pyenv 1>/dev/null 2>&1
  pyenv init - | source
end

source ~/.asdf/asdf.fish
