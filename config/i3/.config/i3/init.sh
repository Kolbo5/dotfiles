#!/bin/sh

SCRIPT_DIR="$( dirname -- "$( readlink -f -- "$0"; )"; )"

echo "On xrandr..."
xrandr_auto.sh

echo "On met les fond d'écrans"
feh --bg-scale /home/eolivier/Images/fond_ecrans/7dfe9d2f3f9c51eb.jpeg --bg-center /home/eolivier/Images/fond_ecrans/fond_ecran_chevaux_paulx.jpg --bg-scale /home/eolivier/Images/fond_ecrans/elephant_Nantes.jpg

echo "On vire polybar avant de relancer"
killall -9 polybar
sleep 2
killall -9 polybar
sleep 2

echo "On bouge les applis dans les bons worspaces"
python3 "$SCRIPT_DIR/i3_switch_windows.py"
sleep 5

echo "On bouge le workspaces"
python3 "$SCRIPT_DIR/i3_switch_workspace.py"
