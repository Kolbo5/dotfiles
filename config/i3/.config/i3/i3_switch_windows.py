#!/usr/bin/env python3

import i3ipc
import os
import subprocess
import time


i3 = i3ipc.Connection()

def debugprint(str):
    with open(f"/tmp/i3logs", 'a') as f:
        f.write(f'{str}\n')
    pass

def launch_app_if_necessary(app_class, launch_command=None):
    debugprint('>> BEGIN launch_app_if_necessary')
    if launch_command is None:
        launch_command = [app_class]
    debugprint(f'Launch "{app_class}" if not launched. With {" ".join(launch_command)}')
    apps = i3.get_tree().find_classed(app_class)
    debugprint(f'{apps=}')
    if len(apps) > 0:
        debugprint(f'App "{app_class}" already launched')
        return
    subprocess.Popen(launch_command)
    #os.system(f'{" ".join(launch_commahostsFrnd)}')
    debugprint('<< END launch_app_if_necessary')

def move_app_to_workspace_number(app_class, ws_num):
    debugprint('>> BEGIN move_app_to_workspace_number')
    debugprint(f'Move "{app_class}" to workspace {ws_num}')
    apps = i3.get_tree().find_classed(app_class)
    if len(apps) == 0:
        debugprint(f'App "{app_class}" not launched')
        return
    apps[0].command(f'move container to workspace number {ws_num}')
    debugprint('<< END move_app_to_workspace_number')

def wait_for_app_launched(app_class):
    debugprint('>> BEGIN wait_for_app_launched')
    while True:
        apps = i3.get_tree().find_classed(app_class)
        debugprint(f'{apps=}')
        if len(apps) > 0:
            debugprint(f'App "{app_class}" LAUNCHED !')
            break
        debugprint(f'App "{app_class}" not launched, waiting...')
        time.sleep(2)
    debugprint('<< END wait_for_app_launched')

def launch_and_move_app(app_class, ws_num, launch_command=None):
    debugprint(f'___________________')
    debugprint(f'launch_and_move_app')
    launch_app_if_necessary(app_class, launch_command)
    if ws_num is not None:
        wait_for_app_launched(app_class)
        move_app_to_workspace_number(app_class, ws_num)
    debugprint(f'-------------------')

def main():
    launch_and_move_app('tgbtray', None)

    launch_and_move_app('firefox', 3)

    launch_and_move_app('KeePassXC', 9, ['keepassxc'])
    launch_and_move_app('Spotify', 9, ['flatpak', 'run', 'com.spotify.Client'])

    launch_and_move_app('Evolution', 10, ['evolution'])
    launch_and_move_app('Mattermost', 10, ['flatpak', 'run', 'com.mattermost.Desktop'])
    launch_and_move_app('TelegramDesktop', 10, ['flatpak','run','org.telegram.desktop'])
    launch_and_move_app('thunderbird', 10)

# Main code
if __name__ == '__main__':
    main()
