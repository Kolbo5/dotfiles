#!/usr/bin/env python3

import i3
import os

DIFFERENTES_CONFIGS = {
    "pc_perso_seul": {
        "left": {
            "name": None,
            "workspaces": None
        },
        "center": {
            "name": "eDP-1-1",
            "workspaces": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        },
        "right": {
            "name": None,
            "workspaces": None
        }
    },
    "pc_perso_sur_bureau_perso": {
        "left": {
            "name": "eDP-1-1",
            "workspaces": [5, 6, 7]
        },
        "center": {
            "name": "DP-4.3",
            "workspaces": [1, 2, 3, 4]
        },
        "right": {
            "name": "DP-4.2",
            "workspaces": [8, 9, 10]
        }
    },
    "pc_perso_sur_bureau_perso_bis": {
        "left": {
            "name": "DP-1-3",
            "workspaces": [5, 6, 7]
        },
        "center": {
            "name": "DP-1-2",
            "workspaces": [1, 2, 3, 4]
        },
        "right": {
            "name": "DP-1-1",
            "workspaces": [8, 9, 10]
        }
    },
    "pc_pro_seul": {
        "left": {
            "name": None,
            "workspaces": None
        },
        "center": {
            "name": "eDP-1",
            "workspaces": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

        },
        "right": {
            "name": None,
            "workspaces": None
        }
    },
    "pc_pro_sur_bureau_pro": {
        "left": {
            "name": "DP-1-3",
            "workspaces": [5, 6, 7]
        },
        "center": {
            "name": "DP-1-2",
            "workspaces": [1, 2, 3, 4]
        },
        "right": {
            "name": "DP-1-2",
            "workspaces": [8, 9, 10]
        }
    },
    "bureaux_lyon": {
        "left": {
            "name": "eDP-1",
            "workspaces": [5, 6, 7]
        },
        "center": {
            "name": "DP-1-2",
            "workspaces": [1, 2, 3, 4]
        },
        "right": {
            "name": "DP-1-1",
            "workspaces": [8, 9, 10]
        }
    },
}

def debugprint(str):
    with open(f"/tmp/i3logs", 'a') as f:
        f.write(f'{str}\n')
    pass

def main():

    debugprint(f'--------------------------------------------------------------------------------------\n')
    outputs = i3.get_outputs()          # get all outputs (used to locate the active outputs available to i3)

    workspaces = i3.get_workspaces()    # get all workspaces (used to locate the output that currently has focus so it can be moved)
    workspaces.sort(key=lambda x: x['num'])
    debugprint(f'workspaces: {workspaces}')

    active_outputs = [o['name'] for o in outputs if o['active'] is True]        # get a list of all active outputs
    active_outputs.sort()
    debugprint(f'active_outputs: {active_outputs}')

    current_workspace = [w['name'] for w in workspaces if w['focused'] is True][0]  # get the current workspace

    workspace_for_actual_config = {}
    for conf_name, conf in DIFFERENTES_CONFIGS.items():
        screen_list_for_this_conf = []
        for screen, screen_config in conf.items():
            screen_name = screen_config.get("name")
            if screen_name is not None:
                screen_list_for_this_conf.append(screen_name)
        workspace_for_actual_config[conf_name] = screen_list_for_this_conf

    actual_config_name = "unknown"
    actual_config = {}
    for config_name, outputs_list in workspace_for_actual_config.items():
        outputs_list.sort()
        if active_outputs == outputs_list:
            actual_config_name = config_name
            actual_config = DIFFERENTES_CONFIGS[config_name]
    debugprint(f'Actual config: {actual_config_name}')

    output_for_workspaces = {}
    for _, screen_config in actual_config.items():
        screen_name = screen_config.get("name")
        screen_workspaces = screen_config.get("workspaces")
        if screen_workspaces is not None:
            for workspace in screen_workspaces:
                output_for_workspaces[workspace] = screen_name
    debugprint(f'{output_for_workspaces=}')

    for i in output_for_workspaces.keys():
        debugprint(f'workspace {i} vers {output_for_workspaces[i]}\n')
        i3.command(f'[workspace={i}] move', f'workspace to output {output_for_workspaces[i]}')
        os.system(f'sed -i "/^workspace \$ws{i}\ / c \\workspace \$ws{i} output {output_for_workspaces[i]}" $HOME/.config/i3/config')
    i3.command('workspace', f'{current_workspace}')
    i3.command('restart')

# Main code
if __name__ == '__main__':
    main()