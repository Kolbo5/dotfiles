#!/bin/sh
xrandr  --output eDP-1 --off \
        --output HDMI-1 --off \
        --output DP-1 --off \
        --output DP-2 --off \
        --output DP-1-1 --mode 1920x1080 --pos 3840x0 --rotate right \
        --output DP-1-2 --primary --mode 1920x1080 --pos 1920x491 --rotate normal \
        --output DP-1-3 --mode 1920x1080 --pos 0x581 --rotate normal
