#!/bin/sh
xrandr \
    --output eDP-1-1 --primary --mode 1920x1080 \
    --output DP-3 --off \
    --output DP-4.1 --off \
    --output DP-4.2 --off \
    --output DP-4.3 --off