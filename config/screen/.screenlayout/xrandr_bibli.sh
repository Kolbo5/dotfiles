#!/bin/sh
xrandr \
    --output eDP-1-1 --mode 1920x1080 \
    --output DP-3 --primary --rotate normal --mode 1440x900 --right-of eDP-1-1 \
	--output DP-4.1 --off \
    --output DP-4.2 --off \
	--output DP-4.3 --off