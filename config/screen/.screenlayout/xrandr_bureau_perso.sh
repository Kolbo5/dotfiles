#!/bin/sh
xrandr --output DP-4.1 --primary --mode 1680x1050 --pos 0x424 --rotate normal --output DP-4.2 --mode 1920x1080 --pos 3360x0 --rotate left --output DP-4.3 --mode 1680x1050 --pos 1680x424 --rotate normal --output DP-0 --off --output DP-1 --off --output DP-2 --off --output DP-3 --off --output DP-4 --off --output DP-5 --off --output eDP-1-1 --off
