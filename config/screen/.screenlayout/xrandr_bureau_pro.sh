#!/bin/sh
xrandr --output eDP-1 --off \
    --output HDMI-1 --mode 1920x1080 --pos 3360x0 --rotate left \
    --output DP-1 --off \
    --output DP-2 --off \
    --output DP-2-1 --off \
    --output DP-2-2 --primary --mode 1680x1050 --pos 1680x577 --rotate normal \
    --output DP-2-3 --mode 1680x1050 --pos 0x577 --rotate normal
