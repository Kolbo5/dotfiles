#!/bin/sh
# xrandr \
#     --output eDP-1 --off \
#     --output HDMI-1 --off \
#     --output DP-1 --off \
#     --output DP-2 --off \
#     --output DP-1-1 --primary --mode 1680x1050 --pos 1680x577 --rotate normal \
#     --output DP-1-2 --mode 1920x1080 --pos 3360x0 --rotate left \
    # --output DP-1-3 --mode 1680x1050 --pos 0x577 --rotate normal

xrandr --output eDP-1 --off \
    --output HDMI-1 --off \
    --output DP-1 --off \
    --output DP-2 --off \
    --output DP-1-1 --mode 1680x1050 --pos 1920x529 --rotate normal \
    --output DP-1-2 --primary --mode 1920x1080 --pos 3600x0 --rotate left \
    --output DP-1-3 --mode 1920x1080 --pos 0x514 --rotate normal
