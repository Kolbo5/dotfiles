" VIM Configuration - Vincent Jousse
" Annule la compatibilite avec l'ancetre Vi : totalement indispensable
set nocompatible

" -- Affichage
set title               " Met a jour le titre de votre fenetre ou de
                        " votre terminal
set number              " Affiche le numero des lignes
set ruler               " Affiche la position actuelle du curseur
set wrap                " Affiche les lignes trop longues sur plusieurs lignes
set scrolloff=3         " Affiche un minimum de 3 lignes autour du curseur
                        " (pour le scroll)

" -- Recherche
set ignorecase          " Ignore la casse lors d'une recherche
set smartcase           " Si une recherche contient une majuscule,
                        " re-active la sensibilite a la casse
set incsearch           " Surligne les resultats de recherche pendant la saisie
set hlsearch            " Surligne les resultats de recherche

" -- Beep
set visualbell          " Empeche Vim de beeper
set noerrorbells        " Empeche Vim de beeper

" Active le comportement 'habituel' de la touche retour en arriere
set backspace=indent,eol,start

" Cache les fichiers lors de l'ouverture d'autres fichiers
set hidden

" Active la coloration syntaxique
syntax enable

" Active les comportements specifiques aux types de fichiers comme
" la syntaxe et l'indentation
filetype on 		" Enable type file detection. Vim will be able
			" to try to detect the type of file in use.
filetype plugin on	" Enable plugins and load plugin for the detected file type.
filetype indent on	" Load an indent file for the detected file type

" Utilise la version sombre de Solarized
set background=dark
colorscheme molokai

" PLUGINS ---------------------------------------------------------------- {{{

call plug#begin('~/.vim/plugged')

  " ALE (Asynchronous Lint Engine) is a plugin providing linting 
  " (syntax checking and semantic errors) in NeoVim 0.6.0+ and 
  " Vim 8.0+ while you edit your text files, and acts 
  " as a Vim Language Server Protocol client
"  Plug 'dense-analysis/ale'

  " The NERDTree is a file system explorer for the Vim editor. 
  " Using this plugin, users can visually browse complex directory 
  " hierarchies, quickly open files for reading or editing, 
  " and perform basic file system operations.
  Plug 'preservim/nerdtree'

  " This small script modifies Vim’s indentation behavior 
  " to comply with PEP8 and my aesthetic preferences.
"  Plug 'Vimjas/vim-python-pep8-indent'

  " Make your Vim/Neovim as smart as VS Code
"  Plug 'neoclide/coc.nvim', {'branch': 'release'}

  " A Vim plugin which shows a git diff in the sign column. 
  " It shows which lines have been added, modified, or removed. 
  " You can also preview, stage, and undo individual hunks; 
  " and stage partial hunks. The plugin also provides a hunk text object.
"  Plug 'airblade/vim-gitgutter'

" Plugin Fuzzy File search
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

call plug#end()

" }}}

" NERDTree PLUGIN ---------------------------------------------------------------- {{{

" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if winnr() == winnr('h') && bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif

" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" NERDTree keymap
nnoremap <leader>n :NERDTreeFocus<CR>
nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
nnoremap <C-f> :NERDTreeFind<CR>

" }}}

" Other PLUGIN ---------------------------------------------------------------- {{{

" pass...

" }}}


" VIMSCRIPT -------------------------------------------------------------- {{{

" This will enable code folding.
" Use the marker method of folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" }}}

